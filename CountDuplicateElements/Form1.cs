namespace CountDuplicateElements
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            string value = this.textBox1.Text;
            if (value.ToString() != "")
            {
                int n = Convert.ToInt16(value);
                int[] numbers = new int[n];
                Numbers num = new Numbers(numbers);
                string number = "";
                for (int i = 0; i < n; i++)
                {

                    number = Prompt.ShowDialog("Mensaje", "Ingrese valor:");
                    numbers[i] = Convert.ToInt32(number);

                }
                this.result.Text = Convert.ToString(num.countDuplicate()); ;
            }
            else 
            {
                MessageBox.Show("Ingrese un valor", "Mensaje");
            }
            

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
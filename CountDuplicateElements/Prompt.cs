﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountDuplicateElements
{
    public static class Prompt
    {
        public static string ShowDialog(string text, string caption)
        {
            Form Prompt = new Form()
            {
                Width = 500,
                Height = 250,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 50, Top = 20, Text = text };
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400 };
            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 90, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { Prompt.Close(); };
            Prompt.Controls.Add(textBox);
            Prompt.Controls.Add(confirmation);
            Prompt.Controls.Add(textLabel);
            Prompt.AcceptButton = confirmation;

            return Prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }
    }
}

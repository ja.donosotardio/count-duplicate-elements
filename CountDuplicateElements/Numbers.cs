﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountDuplicateElements
{
    public class Numbers
    {
        private int[] numbers;
        private int[] numbMarker;
        List<int> objList = new List<int>();
        public Numbers(int[] numbers) { 
          this.numbers = numbers;
            this.numbMarker = new int[numbers.Length];
        }
        public int countDuplicate() 
        {

            int contador = 0;
            for (int i = 0; i < this.numbers.Length; i++)
            {
                if (frecuencia(this.numbers[i]) > 1) 
                {
                    if (!isMarked(this.numbers[i])) 
                    {
                        markedNumber(this.numbers[i]);
                        contador++;
                    }
                }       
            }

            return contador;
        }

        public int frecuencia(int value) 
        {
            int c = 0;
            for (int i = 0; i < this.numbers.Length; i++)
            {
                if (this.numbers[i] == value) 
                {
                  c++;
                }
            }
            return c;
        }
        public void markedNumber(int value) {
           
            objList.Add(value);
        }

        public bool isMarked(int value) 
        {
            for (int i = 0; i < objList.Count; i++)
            {
                if(objList[i] == value)
                    return true;
            }
            return false;
        }

    }
}
